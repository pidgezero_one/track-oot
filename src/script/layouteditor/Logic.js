import SettingsStorage from "/script/storage/SettingsStorage.js";
import GlobalData from "/script/storage/GlobalData.js";

// TODO
class EditorLayout {

    async patch(logic) {
    }

    async clear() {
    }

    async set(type, key, logic) {
    }

    async get(type, key) {
    }

    async remove(type, key) {
    }

}

export default new EditorLayout;